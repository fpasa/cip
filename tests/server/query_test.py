import pytest

from server import db, query


@pytest.mark.parametrize('spec, expected', [
    ('1', [db.Version.version == '1']),
    ('>=1', [db.Version.version >= '1']),
    ('>1', [db.Version.version > '1']),
    ('<=1', [db.Version.version <= '1']),
    ('<1', [db.Version.version < '1']),
    ('1 => 2', [db.Version.version >= '1', db.Version.version < '2']),
])
def test_build_version_filter_single(spec, expected):
    db_filter_list = query.build_version_filter_single(spec)

    assert len(db_filter_list) == len(expected)

    for db_filter, exp_filter in zip(db_filter_list, expected):
        assert db_filter.compare(exp_filter)


@pytest.mark.parametrize('spec, expected', [
    (['1', '2'], [db.Version.version == '1', db.Version.version == '2']),
    (['>=1', '<2'], [db.Version.version >= '1', db.Version.version < '2']),
    (['1 => 2', '>=1.4'], [
        db.Version.version >= '1',
        db.Version.version < '2',
        db.Version.version >= '1.4'
    ]),
])
def test_build_version_filter(spec, expected):
    db_filter_list = query.build_version_filter(spec)

    assert len(db_filter_list) == len(expected)

    for db_filter, exp_filter in zip(db_filter_list, expected):
        assert db_filter.compare(exp_filter)
