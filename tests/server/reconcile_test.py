import pytest
from unittest.mock import Mock

from server.utils import version_range
from server.reconcile import Reconciliator, DependencyError


class queryMock:
    DATA = {
        'a': {
            '1.8.0': {
                'c': '>=14.0.0',
            },
            '2.1.0': {
                'c': '>=18.0.0',
                'd': '>=1.5.0',
            },
            '2.2.0': {
                'c': '>=18.0.0',
                'd': '>=1.6.0',
            },
        },
        'b': {
            '2.8.0': {
                'd': '1.5.0',
            },
        },
        'c': {
            '20.3.13': {}
        },
        'd': {
            '1.5.0': {},
            '1.5.1': {},
            '1.6.0': {},
        },
    }

    @staticmethod
    def wrap(name, ver, deps):
        p = Mock()
        p.name = name

        v = Mock()
        v.transform = lambda: None
        v.version = ver
        v.dependencies = deps

        return p, v

    @staticmethod
    def _select(versions, op, value):
        if not op:
            return versions

        if op == '==':
            return [v for v in versions if v == value]
        elif op == '<=':
            return [v for v in versions if v <= value]
        elif op == '>=':
            return [v for v in versions if v >= value]
        elif op == '<':
            return [v for v in versions if v < value]
        elif op == '>':
            return [v for v in versions if v > value]

    @staticmethod
    def package_version(package, version):
        pkg = queryMock.DATA[package]

        if not isinstance(version, list):
            version = [version]

        versions = pkg.keys()

        for v in version:
            op_from, ver_from, op_to, ver_to = version_range(v)

            versions = queryMock._select(versions, op_from, ver_from)
            versions = queryMock._select(versions, op_to, ver_to)

        if len(versions):
            ver = sorted(versions)[-1]
            return queryMock.wrap(package, ver, pkg[ver])
        else:
            return None

    @staticmethod
    def package(package):
        return package in queryMock.DATA


@pytest.mark.parametrize('spec, expected', [
    # Simple case (one configuration
    # satisfies all constrains)
    ({
        'a': '<2',
        'b': '<3',
    }, {
        'a': '1.8.0',
        'b': '2.8.0',
        'c': '20.3.13',
        'd': '1.5.0',
    }),
    # Case where we have to downgrade
    # a dependency for its dependencies
    # to satisfy the constrain of other
    # peer dependencies

    # RIGHT NOW NOT SUPPORTED

    # ({
    #     'a': '>=2',
    #     'b': '<3',
    # }, {
    #     'a': '2.1.0',
    #     'b': '2.8.0',
    #     'c': '20.3.13',
    #     'd': '1.5.0',
    # }),
])
def test_reconciliator(spec, expected):
    rec = Reconciliator(spec, queryMock)
    deps = rec.reconcile()

    assert deps == expected


@pytest.mark.parametrize('spec', [
    # Complex case: maybe in the future il will pass
    {
        'a': '>=2',
        'b': '<3',
    },
    # Error message is correct even if no parent
    # (conflicting dependencies in spec)
    {
        'b': '<3',
        'd': '>=1.6',
    },
])
def test_reconciliator_error(spec):
    with pytest.raises(DependencyError) as err:
        Reconciliator(spec, queryMock).reconcile()

    assert 'None' not in str(err.value)
    assert '@' in str(err.value)
