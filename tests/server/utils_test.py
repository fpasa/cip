import pytest

from server import utils


@pytest.mark.parametrize('ver, ex_out', [
    ('1', ('==', '1', None, None)),
    (' >= 1 ', ('>=', '1', None, None)),
    ('>1 ', ('>', '1', None, None)),
    ('<= 1 ', (None, None, '<=', '1')),
    (' < 1 ', (None, None, '<', '1')),
    (' 1 => 2 ', ('>=', '1', '<', '2')),
])
def test_version_range(ver, ex_out):
    out = utils.version_range(ver)
    assert out == ex_out
