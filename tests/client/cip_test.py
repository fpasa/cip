import os
import json
import urllib.error
import pytest
from unittest.mock import patch

from client import cip

PACKAGE_110 = {
    "created_on": "2019-09-02T20:54:45",
    "homepage": None,
    "license": None,
    "name": "test",
    "repository": None,
    "updated_on": "2019-09-02T20:54:45",
    "version": {
        "created_on": "2019-09-02T20:54:45",
        "dependencies": {},
        "description": None,
        "package_id": 1,
        "updated_on": "2019-09-02T20:54:45",
        "version": "1.1.0"
    }
}
PACKAGE_103 = {
    "created_on": "2019-09-02T20:54:45",
    "homepage": None,
    "license": None,
    "name": "test",
    "repository": None,
    "updated_on": "2019-09-02T20:54:45",
    "version": {
        "created_on": "2019-09-02T20:54:45",
        "dependencies": {},
        "description": None,
        "package_id": 1,
        "updated_on": "2019-09-02T20:54:45",
        "version": "1.0.3"
    }
}


class HttpErrorMock(urllib.error.HTTPError):
    def __init__(self, status):
        self.status = status


@pytest.fixture
def tmp_context():
    class TmpContext:
        def __init__(self, path):
            self.path = path

        def __enter__(self):
            self.orig_dir = os.getcwd()
            os.chdir(self.path)

        def __exit__(self, exc_type, exc_val, exc_tb):
            os.chdir(self.orig_dir)

    return TmpContext


@pytest.fixture
def server():
    def request(method, url, data=None):
        body, err = {
            'GET_/api/package/test': (PACKAGE_110, None),
            'GET_/api/package/test/>=1': (PACKAGE_110, None),
            'GET_/api/package/test/1.0.3': (PACKAGE_103, None),
            'GET_/api/package/test/<1.1.0': (PACKAGE_103, None),
            'GET_/api/package/does_not_exist':
                ({"error": "not found"}, HttpErrorMock(404)),
            'GET_/api/package/test/1@123':
                ({"error": "not found"}, HttpErrorMock(404)),
            'POST_/api/reconcile': ({"test": "1.1.0"}, None),
        }[f'{method}_{url}']

        if err:
            raise err

        return body

    with patch('client.cip.request', request):
        yield


@pytest.mark.parametrize('inp, outp', [
    ([], {}),
    (['init', '.'], {'directory': '.'}),
])
def test_parse_args(inp, outp):
    args = vars(cip.parse_args(inp))

    if 'func' in args:
        del args['func']

    assert args == outp


@pytest.mark.parametrize('args, code', [
    ([], 0),
    (['does_not_exist'], 2),
])
def test_main(args, code):
    with pytest.raises(SystemExit) as ex:
        cip.main(args)

    print(ex.value == 2, ex.value.code == 2, code == 2)
    assert ex.value.code == code


@pytest.mark.parametrize('conf, ex', [
    (None, SystemExit),
    ('asd', SystemExit),
    ('{}', Exception),
    ('{"a": "property",}', SystemExit),
    ('{"a": "property"}', Exception),
    ('[]', SystemExit),
    ('true', SystemExit),
    ('123', SystemExit),
])
def test_parse_conf_errors(conf, ex, tmp_path, tmp_context):
    if conf:
        conf_path = os.path.join(tmp_path, cip.PKG_FILE)

        with open(conf_path, 'w') as fp:
            fp.write(conf)

    with tmp_context(tmp_path):
        with pytest.raises(ex):
            read_conf = cip.parse_conf()
            assert read_conf == conf


def test_init_package(tmp_path):
    cip.init_package(tmp_path, interactive=False)

    file = os.path.join(tmp_path, cip.PKG_FILE)
    with open(file) as fp:
        assert json.load(fp) == cip.INIT_CONF


@pytest.mark.parametrize('directory, ex', [
    ('asd', SystemExit),
    ('cip.py', SystemExit),
])
def test_init_package_errors(directory, ex):
    with pytest.raises(ex):
        cip.init_package(directory)


@pytest.mark.parametrize('conf_in, pkg, conf_out', [
    # test setting property
    ({}, 'test', {'requires': {'test': '1.1.0'}}),
    ({'requires': {}}, 'test',
        {'requires': {'test': '1.1.0'}}),
    ({'requires': {'another': '1.2.3'}}, 'test',
        {'requires': {'another': '1.2.3', 'test': '1.1.0'}}),
    # with version
    ({}, 'test@1.0.3', {'requires': {'test': '1.0.3'}}),
    # try breaking it
    ({}, 'test@>=1', {'requires': {'test': '1.1.0'}}),
    ({}, 'test@<1.1.0', {'requires': {'test': '1.0.3'}}),
])
def test_add_package(conf_in, pkg, conf_out, server):
    cip.add_package(conf_in, pkg)
    assert conf_in == conf_out


@pytest.mark.parametrize('pkg, err, contains', [
    ('does_not_exist', SystemExit, 'package "does_not_exist" does not exist'),
    ('test@1@123', SystemExit, 'package "test" version 1@123 does not exist'),
])
def test_add_package_error(pkg, err, contains, capsys, server):
    with pytest.raises(err):
        cip.add_package({}, pkg)

    captured = capsys.readouterr()
    assert contains in captured.err


@pytest.mark.parametrize('conf, lock', [
    # test setting property
    ({}, {}),
    ({'requires': {'test': '>1.0.3'}}, {'test': '1.1.0'}),
])
def test_sync(conf, lock, tmp_path, tmp_context, server):
    with tmp_context(tmp_path):
        cip.sync(conf)
        with open(cip.LOCK_FILE) as fp:
            assert json.load(fp) == lock


# @pytest.mark.parametrize('conf', [
#     # test setting property
#     {},
# ])
# def test_sync_error(conf, server):
#     cip.sync(conf)

#     server.terminate()
