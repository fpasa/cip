def version_range(version):
    if '=>' in version:
        ver_from, ver_to = [v.strip() for v in version.split('=>')]
        op_from = '>='
        op_to = '<'
    elif '>=' in version:
        ver_from, ver_to = version.replace('>=', '').strip(), None
        op_from, op_to = '>=', None
    elif '<=' in version:
        ver_from, ver_to = None, version.replace('<=', '').strip()
        op_from, op_to = None, '<='
    elif '>' in version:
        ver_from, ver_to = version.replace('>', '').strip(), None
        op_from, op_to = '>', None
    elif '<' in version:
        ver_from, ver_to = None, version.replace('<', '').strip()
        op_from, op_to = None, '<'
    else:
        ver_from, ver_to = version, None
        op_from, op_to = '==', None

    return op_from, ver_from, op_to, ver_to
