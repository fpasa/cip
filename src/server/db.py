import os
import datetime
import json

from flask import jsonify
from sqlalchemy import create_engine, func
from sqlalchemy import Column, String, Integer, DateTime, ForeignKey, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

CIP_DATABASE = os.environ.get(
    'CIP_DATABASE', 'cip.sqlite')
CIP_PACKAGE_FOLDER = os.environ.get(
    'CIP_PACKAGE_FOLDER', os.path.abspath('cip'))

ENGINE = None

# Base "class" for tables
Base = declarative_base()


# def is_test():
#     return (
#         'pytest' in sys.modules
#         or 'TEST' in os.environ and os.environ['TEST']
#     )


class Package(Base):
    __tablename__ = 'packages'

    id = Column(Integer, primary_key=True)
    created_on = Column(DateTime, server_default=func.now())
    updated_on = Column(
        DateTime,
        server_default=func.now(),
        onupdate=func.now()
    )

    name = Column(String)
    license = Column(String)
    repository = Column(String)
    homepage = Column(String)

    versions = relationship('Version', back_populates='package')


class Version(Base):
    __tablename__ = 'versions'

    id = Column(Integer, primary_key=True)
    created_on = Column(DateTime, server_default=func.now())
    updated_on = Column(
        DateTime,
        server_default=func.now(),
        onupdate=func.now()
    )

    version = Column(String)
    description = Column(Text)

    package_id = Column(Integer, ForeignKey(Package.id))
    package = relationship('Package', back_populates='versions')

    dependencies = Column(Text)

    def transform(self):
        deps = self.dependencies
        self.dependencies = json.loads(deps) if deps else {}


def _get_engine():
    global ENGINE

    if ENGINE:
        return ENGINE

    # if is_test():
    #     ENGINE = create_engine(f'sqlite://', echo=True)
    # else:
    ENGINE = create_engine(f'sqlite:///{CIP_DATABASE}')

    return ENGINE


def create_schema():
    engine = _get_engine()
    Base.metadata.create_all(engine)


def get_session():
    engine = _get_engine()
    Session = sessionmaker(bind=engine)
    return Session()


def serialize(obj):
    if isinstance(obj, list):
        return [serialize(s) for s in obj]

    if isinstance(obj, tuple):
        return {
            s.__class__.__name__.lower(): serialize(s)
            for s in obj
        }

    getattr(obj, 'transform', lambda: None)()
    obj = dict(vars(obj))

    for key in obj:
        if isinstance(obj[key], datetime.datetime):
            obj[key] = obj[key].isoformat()

    del obj['_sa_instance_state']
    del obj['id']

    return obj


def to_json(obj):
    return jsonify(serialize(obj))


# if is_test():
#     CIP_PACKAGE_FOLDER = tempfile.TemporaryDirectory().name
#
#     create_schema()
#
#     session = get_session()
#
#     pkg_test = Package(name='test')
#     pkg_test_103 = Version(version='1.0.3', package=pkg_test)
#     pkg_test_110 = Version(version='1.1.0',  package=pkg_test)
#
#     session.add_all([
#         pkg_test,
#         pkg_test_103, pkg_test_110,
#     ])
#
#     session.commit()
