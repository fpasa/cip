from . import db
from .utils import version_range


def all_package_versions(package):
    session = db.get_session()

    return (
        session
        .query(db.Package, db.Version)
        .filter(db.Package.name == package)
        .join(db.Version)
    ).all()


def package(package):
    session = db.get_session()

    return (
        session
        .query(db.Package, db.Version)
        .filter(db.Package.name == package)
    ).first()


def package_version(package, version_spec):
    session = db.get_session()

    version_filter = build_version_filter(version_spec)

    return (
        session
        .query(db.Package, db.Version)
        .filter(db.Package.name == package)
        .join(db.Version)
        .filter(*version_filter)
        .order_by(db.Version.version.desc())
    ).first()


def build_filter(op, value):
    if op == '==':
        return db.Version.version == value
    elif op == '<=':
        return db.Version.version <= value
    elif op == '>=':
        return db.Version.version >= value
    elif op == '<':
        return db.Version.version < value
    elif op == '>':
        return db.Version.version > value


def build_version_filter_single(version_spec):
    op_from, ver_from, op_to, ver_to = version_range(version_spec)

    version_filter = []
    if op_from:
        version_filter.append(build_filter(op_from, ver_from))
    if op_to:
        version_filter.append(build_filter(op_to, ver_to))

    return version_filter


def build_version_filter(version_spec):
    if not isinstance(version_spec, list):
        version_spec = [version_spec]

    version_filter = []
    for vs in version_spec:
        version_filter.extend(
            build_version_filter_single(vs))

    return version_filter
