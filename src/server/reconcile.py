from . import query


class NotFoundError(Exception):
    pass


class DependencyError(Exception):
    pass


class Reconciliator:
    def __init__(self, package_spec, _query=query):
        self._package_spec = package_spec
        self._dependencies = {}
        self._query = _query

    def _add_dependency(self, dep, ver):
        if dep not in self._dependencies:
            self._dependencies[dep] = []

        self._dependencies[dep].append(ver)

    def _get_package_dependencies(self, package, version):
        if self._query.package(package) is None:
            raise NotFoundError(
                f'package "{package}" does not exist')

        db_package_version = self._query.package_version(package, version)

        if db_package_version is None:
            raise NotFoundError(
                f'version {version} of package "{package}" does not exist')

        db_package, db_version = db_package_version
        db_version.transform()

        return db_package, db_version, db_version.dependencies

    def _prepare_dep_constrains(self):
        for package, version in self._package_spec.items():
            self._add_dependency(package, {
                'version': version,
                'parent': None,
            })

            db_package, db_version, package_deps = (
                self._get_package_dependencies(package, version))

            for dep_package, dep_versions in package_deps.items():
                self._add_dependency(dep_package, {
                    'version': dep_versions,
                    'parent': db_package,
                    'parent_version': db_version,
                })

    def _satisfy_dep_constrains(self, dep, constrains):
        print(dep, constrains)
        db_package_version = self._query.package_version(
            dep,
            [c['version'] for c in constrains]
        )
        print(db_package_version)

        if db_package_version is None:
            conflicts = []
            for c in constrains:
                if c['parent']:
                    parent_name = c["parent"].name
                    parent_version = c["parent_version"].version
                    parent = parent_name + '@' + parent_version
                    req_version = c['version']
                    conflicts.append(
                        f'{parent} requires {dep} version {req_version}')
                else:
                    req_version = c['version']
                    conflicts.append(
                        f'{dep} version {req_version} required in "cip.json"')

            raise DependencyError(
                f'Impossible to satisfy dependencies:\n'
                + '\n'.join([f'    {c}' for c in conflicts])
            )

        db_package, db_version = db_package_version

        self._dependencies[dep] = db_version.version

    def _satisfy_constrains(self):
        for dep, constrains in self._dependencies.items():
            self._satisfy_dep_constrains(dep, constrains)

    def reconcile(self):
        self._prepare_dep_constrains()

        self._satisfy_constrains()

        return self._dependencies
