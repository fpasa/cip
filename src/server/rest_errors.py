from flask import jsonify


def not_found(desc=None):
    return jsonify({
        'error': 'not found',
        **({'description': desc} if desc else {})
    }), 404


def bad_request(desc):
    return jsonify({
        'error': 'bad request',
        **({'description': desc} if desc else {})
    }), 400
