from flask import Flask, jsonify, request
from rest_errors import not_found, bad_request
from db import serialize, create_schema
import query
from reconcile import Reconciliator, NotFoundError

create_schema()
app = Flask(__name__)


@app.route('/api/package/<package>/versions', methods=['GET'])
def get_package_versions(package):
    db_package_versions = query.all_package_versions(package)

    if not db_package_versions:
        return not_found()

    # At least one version must exist, otherwise no package
    db_package = db_package_versions[0][0]
    # extract versions
    db_versions = [ver[1] for ver in db_package_versions]

    return jsonify({
        **serialize(db_package),
        'versions': serialize(db_versions)
    })


@app.route('/api/package/<package>', methods=['GET'])
def get_package(package):
    return get_package_version(package, '>=0')


@app.route('/api/package/<package>/<version>', methods=['GET'])
def get_package_version(package, version):
    db_package_version = query.package_version(package, version)

    if db_package_version is None:
        return not_found()

    db_package, db_version = db_package_version

    return jsonify({
        **serialize(db_package),
        'version': serialize(db_version)
    })


@app.route('/api/reconcile', methods=['POST'])
def reconcile():
    body = request.get_json()

    if not isinstance(body, dict):
        return bad_request(
            'the body must be json object in the format: '
            '{"package": "version", ...}'
        )

    try:
        rec = Reconciliator(body)
        deps = rec.reconcile()
    except NotFoundError as ex:
        return not_found(str(ex))

    return jsonify(deps)

    # constrains = []
    # for package, version in body.items():
    #     ver_from, ver_to = version_range(version)

    #     if ver_from:
    #         version_filter.append(db.Version.version >= ver_from)
    #     if ver_to:
    #         version_filter.append(db.Version.version < ver_to)

    #     db_package_version = query.package_version(package, version)

    # session = db.get_session()
    # package = (
    #     session
    #     .query(db.Package, db.Version)
    #     .filter(db.Package.name == name)
    #     .join(db.Version)
    #     .filter(db.Version.version == version)
    # ).first()

    # if package is None:
    #     return jsonify({
    #         'error': 'not found',
    #     }), 404

    # return jsonify({
    #     **db.serialize(package[0]),
    #     'version': db.serialize(package[1])
    # })

# @app.route('/package/<name>/<version>', methods=['POST'])
# def get_package(name, version):
    # session = db.get_session()
    # package = (
    #     session
    #     .query(db.Package)
    #     .filter(db.Package.name == name)
    # ).first()

    # if package is None:
    #     return jsonify({
    #         'error': 'not found',
    #     }), 404

    # return jsonify(package)
