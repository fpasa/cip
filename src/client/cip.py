import os
import sys
import json
import argparse
import urllib.request
from typing import List

PKG_FILE = 'pkg.json'
LOCK_FILE = '.pkg-lock.json'

SERVER = 'http://127.0.0.1:5000'
INIT_CONF = {
    'name': '',
    'version': '',
    'license': '',
    'requires': {},
    'provides': {},
}


def print_err(err):
    print(f'Error: {err}', file=sys.stderr)


def request(method, url, data=None):
    req = urllib.request.Request(
        f'{SERVER}{url}',
        json.dumps(data).encode('utf-8'),
        headers=({'Content-Type': 'application/json'} if data else {}),
        method=method
    )

    with urllib.request.urlopen(req) as res:
        return json.loads(res.read())


def init_package(directory, interactive=True):
    directory = os.path.normpath(
        os.path.join(os.getcwd(), directory))

    if not os.path.isdir(directory):
        print_err(f'"{directory}" is not a directory.')
        sys.exit(1)

    file = os.path.join(directory, PKG_FILE)

    if os.path.exists(file):
        # Already initialized, do nothing
        return print(f'Directory "{directory}" already initialized')

    conf = INIT_CONF
    if interactive:
        name = os.path.basename(directory)
        conf['name'] = input(f'Name ({name}): ') or name
        conf['version'] = input(f'Version (1.0.0): ') or '1.0.0'
        conf['license'] = input(f'License (MIT): ') or 'MIT'

    save_conf(conf, file)


def add_packages(conf, pkgs):
    for pkg in pkgs:
        add_package(conf, pkg)

    sync(conf)


def add_package(conf, pkg_spec):
    dep_list = conf['requires'] if 'requires' in conf else {}

    if '@' in pkg_spec:
        pkg_spec_name, pkg_spec_ver = pkg_spec.split('@', 1)

        try:
            pkg = request(
                'GET',
                f'/api/package/{pkg_spec_name}/{pkg_spec_ver}'
            )
        except urllib.error.HTTPError as ex:
            if ex.status == 404:
                print_err(
                    f'package "{pkg_spec_name}" version '
                    f'{pkg_spec_ver} does not exist.')
            else:
                print_err(f'{ex}')

            sys.exit(1)
    else:
        pkg_spec_name = pkg_spec

        try:
            pkg = request('GET', f'/api/package/{pkg_spec_name}')
        except urllib.error.HTTPError as ex:
            if ex.status == 404:
                print_err(f'package "{pkg_spec_name}" does not exist.')
            else:
                print_err(f'{ex}')

            sys.exit(1)

    pkg_ver = pkg['version']['version']
    dep_list[pkg_spec_name] = pkg_ver

    conf['requires'] = dep_list


def del_packages(conf, pkgs):
    for pkg in pkgs:
        del_package(conf, pkg)

    sync(conf)


def del_package(conf, pkg):
    if 'requires' in conf:
        if pkg in conf['requires']:
            del conf['requires'][pkg]
        else:
            print_err(f'package "{pkg}" is not in "pkg.conf"')
            sys.exit(1)


def sync(conf):
    requires = conf.get('requires', {})

    if requires:
        try:
            new_lock = request('POST', f'/api/reconcile', requires)
        except urllib.error.HTTPError as ex:
            print_err(ex)
            sys.exit(1)
    else:
        new_lock = {}

    lock_file = os.path.join(os.getcwd(), LOCK_FILE)
    with open(lock_file, 'w') as fp:
        json.dump(new_lock, fp)

    sync_packages(new_lock)


def sync_packages(lock):
    print(lock)


class SubcommandHelpFormatter(argparse.RawDescriptionHelpFormatter):
    """This class removes the metavar from the subcommand help message."""

    def _format_action(self, action):
        parts = super()._format_action(action)
        if action.nargs == argparse.PARSER:
            parts = "\n".join(parts.split("\n")[1:])
        return parts


def parse_args(args: List[str] = None) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=SubcommandHelpFormatter,
        prog='cip'
    )
    subparsers = parser.add_subparsers(
        title='where <command> is one of the following',
        metavar='<command>'
    )

    # init
    parser_init = subparsers.add_parser('init', help='initialize package')
    parser_init.add_argument(
        'directory', metavar='DIR', type=str,
        help='folder where the package should be initialized'
    )
    parser_init.set_defaults(func=init_package)

    # add
    parser_add = subparsers.add_parser('add', help='add dependency')
    parser_add.add_argument(
        'pkgs', metavar='PACKAGE', nargs='+', type=str,
        help='packages to be added'
    )
    parser_add.set_defaults(func=add_packages)

    # del
    parser_del = subparsers.add_parser('del', help='remove dependency')
    parser_del.add_argument(
        'pkgs', metavar='PACKAGE', nargs='+', type=str,
        help='packages to be removed'
    )
    parser_del.set_defaults(func=del_packages)

    # sync
    parser_del = subparsers.add_parser(
        'sync',
        help=f'install or update dependencies from "{PKG_FILE}"'
    )
    parser_del.set_defaults(func=sync)

    return parser.parse_args(args)


def parse_conf() -> object:
    file = os.path.join(os.getcwd(), PKG_FILE)

    if not os.path.exists(file):
        print_err(f'no "{PKG_FILE}" file found in the current directory.')
        sys.exit(1)

    with open(file) as fp:
        try:
            conf = json.load(fp)
        except ValueError:
            print_err(f'"{PKG_FILE}" file is not a valid json file.')
            sys.exit(1)

    if not isinstance(conf, dict):
        print_err(f'"{PKG_FILE}" file is not a valid package file.')
        sys.exit(1)

    return conf


def save_conf(conf: object, file: str = None):
    if not file:
        file = os.path.join(os.getcwd(), PKG_FILE)

    with open(file, 'w') as fp:
        json.dump(conf, fp, indent=4)


def main(args: List[str] = None):
    args = parse_args(args)

    # If no command is given, print help
    if vars(args) == {}:
        return parse_args(['-h'])

    # The dict is necessary to make a copy,
    # otherwise del will delete the object from args as well
    args_dict = dict(vars(args))
    del args_dict['func']

    # The command 'init' does not need a configuration
    if args.func is init_package:
        return init_package(**args_dict)
    else:
        conf = parse_conf()
        args.func(conf, **args_dict)

    save_conf(conf)


if __name__ == '__main__':
    main()
